# Color Tiles

Draws colorful squares inside a window

![Screenshot](Screenshot.png "screenshot")

## Usage

`python3 Tiles.py`

## Keybinds

| Key | Purpose                                    |
| :-: | :----------------------------------------- |
| q   | Exit program                               |
| w   | Increase tile placement speed              |
| e   | Clear the window                           |
| t   | Manually place a tile                      |
| a   | Toggle manual and automatic tile placement |
| s   | Decrease tile placement speed              |
| d   | Manually place a tile                      |
| c   | Clear the window                           |
| m   | Toggle manual and automatic tile placement |
